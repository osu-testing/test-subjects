.TH CTAGS 1 "Version 2.3" "Darren Hiebert"


.SH NAME
ctags \- Generate C and C++ language tag files for use with
.BR vi (1)


.SH SYNOPSIS
.TP 6
\fBctags\fR
[\fB-aBeFnNRux\fR]
[\fB-f \fItagfile\fR]
[\fB-h \fIlist\fR]
[\fB-i \fItypes\fR]
[\fB-I \fIignorelist\fR]
[\fB-L \fIlistfile\fR]
[\fB-p \fIpath\fR]
[\fB-o \fItagfile\fR]
[\fB--append\fR]
[\fB--excmd=\fIm\fR|\fIn\fR|\fIp\fR]
[\fB--format=\fIlevel\fR]
[\fB--help\fR]
[\fB--if0\fR]
[\fB--lang=\fIc\fR|\fIc++\fR|\fIjava\fR]
[\fB--langmap=\fImap(s)\fR]
[\fB--recurse\fR]
[\fB--sort\fR]
[\fB--totals\fR]
[\fB--version\fR]
[\fIfile(s)\fR]

.br
.br
.TP 6
\fBetags\fP
[\fB-aRx\fR]
[\fB-f \fItagfile\fR]
[\fB-h \fIlist\fR]
[\fB-i \fItypes\fR]
[\fB-I \fIignorelist\fR]
[\fB-L \fIlistfile\fR]
[\fB-p \fIpath\fR]
[\fB-o \fItagfile\fR]
[\fB--append\fR]
[\fB--help\fR]
[\fB--if0\fR]
[\fB--lang=\fIc\fR|\fIc++\fR|\fIjava\fR]
[\fB--langmap=\fImap(s)\fR]
[\fB--recurse\fR]
[\fB--totals\fR]
[\fB--version\fR]
[\fIfile(s)\fR]


.SH DESCRIPTION
The \fBctags\fP and \fBetags\fP programs (hereinafter collectively referred to
as \fBctags\fP, except where distinguished) generate an index (or "tag") file
for C, C++ and Java language objects found in \fIfile(s)\fP that allows these
items to be quickly and easily located by a text editor or other utility. A
"tag" signifies a C language object for which an index entry is available (or,
alternatively, the index entry created for that object).
.PP
Alternatively, \fBctags\fP can generate a cross reference file which lists, in
human readable form, information about the various objects found in a set of C
or C++ language files.
.PP
Tag index files are supported by the \fBvi\fP(1) editor and its derivatives
(such as \fBvim\fP, \fBelvis\fP, \fBstevie\fP, and \fBxvi\fP), and by the
\fBemacs\fP editor (see the \fBHOW TO USE WITH\fP sections, below), all of
which allow the user to locate the object associated with a name appearing in
a source file and jump to the file and line which defines the name.

The following types of tags are supported by \fBctags\fP (some are optional):
.PP
.RS 4
.br
macro definitions (names created by #define)
.br
enumerators (values inside an enumeration)
.br
function (method) definitions
.br
class, enum, struct, and union names
.br
interface names (Java)
.br
namespace names (C++)
.br
typedefs
.br
variables (definitions and extern declarations)
.br
function prototypes and declarations
.br
data members
.RE
.PP
.B Ctags
only generates tags for objects which have global scoping (file-wide
visibility). This means that, with the exception of macro definitions, only
objects defined outside of brace enclosed function blocks are candidates for a
tag.


.SH SOURCE FILES
.PP
Unless the \fB--lang\fP option is specified, the language of each source file
is automatically selected based upon its file extension, according to the
following default mapping.
.PP
.PD 0
.RS 4
.TP 8
C
*.c
.TP 8
C++
*.C, *.c++, *.cc, *.cpp, *.cxx and any file considered a header file according
to the \fB-h\fP option.
.TP 8
Java
*.java
.PD 1
.RE
.PP
All other files extensions are ignored. This permits running \fBctags\fP on
all files in either a single directory (e.g. "ctags *"), or all files in an
entire source directory tree (e.g. "ctags -R"), since only those files whose
extensions are known to \fBctags\fP will be scanned. This default mapping of
file extensions to source language may be changed by using the \fB--langmap\fP
option. Automatic selection of the source language may be disabled by using
the \fB--lang\fP option.


.SH OPTIONS
.PP
Despite the wealth of available options, defaults are set so that \fBctags\fP
is most commonly executed without any options (e.g. "ctags *"), which will
create a tag file in the current directory for the specified files. The
options described below are provided merely to allow custom tailoring to meet
special needs.
.PP
Note that spaces separating the single-letter options from their parameters
are optional.
.PP
Note also that the boolean parameters to the long form options (those
beginning with "--" and that take a "\fB=\fIyes\fR|\fIno\fR" parameter) may
be omitted, in which case "\fB=\fIyes\fR" is implied. (e.g. \fB--sort\fP is
equivalent to \fB--sort=\fIyes\fR).

.TP 5
.B \-a
Append the tags to an existing tag file. Equivalent to \fB--append\fR.

.TP 5
.B \-B
Use backward searching patterns (e.g. ?regexp?).

.TP 5
.B \-e
Output tag file for use with Emacs. If this program is executed by the name
\fBetags\fP, this option is set by default. Selecting this option causes the
following options to be ignored: \fB-BFnNsuwW\fP, \fB--excmd\fP,
\fB--format\fP, \fB--sort\fP.

.TP 5
.BI \-f " tagfile"
Use the name specified by \fItagfile\fP for the tag file (default is "tags",
or "TAGS" when using the \fB-e\fP option). If \fItagfile\fP is specified as
"-", then the tag file is written to standard output instead. \fBCtags\fP
will stubbornly refuse to take orders if \fItagfile\fP exists and its first
line contains something other than a valid tags line. This will save your neck
if you mistakenly type "ctags -f *.c", which would otherwise overwrite your
first C file with the tags generated by the rest!

.TP 5
.B \-F
Use forward searching patterns (e.g. /regexp/) (default).

.TP 5
.BI \-h  " list"
Specifies a list of file extensions used for headers, separated by periods.
This option affects how the scoping of tag types is interpreted (i.e. whether
or not they are considered as static). All tag types other than functions
(methods) and variables are considered static when they appear in a non-header
file (since they cannot be seen outside of that file) and non-static when they
appear in a header file (since they can be seen in other files wherever that
header file is included). See also the \fBS\fP modifier of the  \fB-i\fP
option. The default list is ".h.H.hh.hpp.hxx.h++".

.TP 5
.BI \-i " types"
Specifies the list of tag types to include in the output file.
.I Types
is a group of one-letter flags designating the types of tags affected. Each
letter or group of letters may be preceded by either a '+' sign (default, if
omitted) to add it to those already included, a '-' sign to exclude it from
the list (e.g. to exclude a default tag type), or an '=' sign to include its
corresponding tag type at the exclusion of those not listed. Tags for the
following language contructs are supported (default settings are on except as
noted):
.PP
.RS 8
.PD 0
.TP 4
.I c
class names
.TP 4
.I d
macro definitions
.TP 4
.I e
enumerators
.TP 4
.I f
function (or method) definitions
.TP 4
.I g
enumeration names
.TP 4
.I i
interface names (Java only)
.TP 4
.I m
data members [off]
.TP 4
.I i
namespace names (C++ only)
.TP 4
.I p
function prototypes and declarations [off]
.TP 4
.I s
structure names
.TP 4
.I t
typedefs
.TP 4
.I u
union names
.TP 4
.I v
variable definitions
.TP 4
.I x
extern variable declarations [off]
.RE
.PD 1
.PP
.RS 5
In addition to the above flags, the following one-letter modifiers are
accepted:
.RE
.PP
.PD 0
.RS 8
.TP 4
.I C
For C++ and Java, include an extra, class-qualified tag entry for each class
member (function/method and data) in the form "class::member" (C++) or
"class.member" (Java). This allows locating class-qualified tags (e.g. ":tag
class::member" in \fBvi\fP(1)). This is disabled by default because this could
potentially more than double the size of the tag file.
.RE
.RS 8
.TP 4
.I F
Include tags for the basename of each source file. This permits jumping to a
file by its name (e.g. ":tag file.c" in \fBvi\fP(1)). This is disabled by
default.
.TP 4
.I S
Include static tags (those not visible outside of a single source file).
Function and variable definitions are considered static only when their
definitions are preceded with the "static" keyword. All other types of tags
are considered static when they appear in a non-header file, and non-static
when they appear in a header file. See also the \fB-h\fP option. This is
enabled by default.
.RE
.PD 1
.PP
.RS 5
.PP
The default value for \fItypes\fP is "=cdefgintuvCS".
.RE

.TP 5
.BI \-I " ignorelist"
Reads a list of names which are to be ignored while generating tags for the
source files. The list may be supplied directly on the command line or found
in a separate file. Normally, the parameter
.I ignorelist
is a list of names to be ignored, each separated with a comma, a semicolon, or
white space (in which case the list should be quoted to keep the entire list
as one command line argument). The parameter
.I ignorelist
will be interpreted as a filename if its first character is given as either
a '.' or a pathname separator ('/' or '\\'). In order to specify a file found
in the current directory, use "./filename".
.RS 5
.PP
This feature is useful when preprocessor macros are used in such a way that
they cause syntactic confusion due to their presence. Some examples will
illustrate this point.
.RE
.PP
.RS
/* creates a global version string in module */
MODULE_VERSION("$Revision: 6.10 $")
.RE
.PP
.RS 5
In this example, the macro invocation looks to much like a function definition
because it is not followed by a semicolon (indeed, it could even be followed
by a global variable definition that would look exactly like a K&R style
function parameter declaration). In fact, this seeming function definition
would likely cause the rest of the file to be skipped over while trying to
complete the definition. Ignoring "MODULE_ID" would avoid such a problem.
.RE
.PP
.RS
int foo ARGDECL2(void *, ptr, long int, nbytes)
.RE
.PP
.RS 5
In this example, the macro "ARGDECL2" would be mistakenly interpreted to be
the name of the function instead of the correct name of "foo". Ignoring the
name "ARGDECL2" results in the correct behavior.
.RE

.TP 5
.BI \-L " listfile"
Read from
.I listfile
a list of file names for which tags should be generated. If
.I listfile
is specified as "-", then file names are read from standard input.

.TP 5
.B \-n
Equivalent to \fB--excmd\fI=number\fR.

.TP 5
.B \-N
Equivalent to \fB--excmd\fI=pattern\fR.

.TP 5
.BI \-o " tagfile"
Alternative for \fB-f\fP.

.TP 5
.BI \-p " path"
Use \fIpath\fP as the default directory for each supplied source file (whether
supplied on the command line or in a file specified with the \fB-L\fP option),
unless the source file is already specified as an absolute path. The supplied
\fIpath\fP is merely prepended to the each non-absolute source file name,
adding any necessary path separator.

.TP 5
.B \-R
Equivalent to \fB--recurse\fI=yes\fR.

.TP 5
.B \-u
Equivalent to \fB--sort\fI=no\fR.

.TP 5
.B \-x
Print a tabular, human-readable cross reference (xref) file to standard output
instead of generating a tag file. The information contained in the output
includes: the tag name; the kind of tag; the line number, file name, and
source line (with extra white space condensed) of the file which defines the
tag. No tag file is written and the following options will be ignored:
\fB-aBefFno\fP, and \fB-i\fP\fI+P\fP. Example applications for this feature are
generating a listing of all functions (including statics) located in a source
file (e.g. \fBctags -xi\fP\fI=fS\fP \fIfile\fP), or generating a list of all
externally visible global variables located in a source file (e.g. \fBctags
-xi\fP\fI=v file\fP).

.TP 5
\fB--append=\fIyes\fR|\fIno\fR
Indicates whether tags generated from the specified files should be appended
to those already present in the tag file or should replace them. If the
parameter is omitted, \fB=\fIyes\fR is implied. This option is off by default.

.TP 5
.BI \--excmd= type
Determines the type of EX command used to locate tags in the source file. The
valid values for \fItype\fP are (either the entire word or the first letter is
accepted):
.RS 5
.TP 9
.I number
Places into the tag file line numbers in the source file where tags are located
rather than patterns to be searched for. This has three advantages:
.PD 0
.RS 9
.TP 4
1.
Significantly reduces the size of the resulting tag file.
.TP 4
2.
Eliminates failures to find tags because the line defining the tag has
changed, causing the pattern match to fail (note that some editors, such as
\fBvim\fP, are able to recover in many such instances).
.TP 4
3.
Eliminates finding identical matching, but incorrect, source lines (see
\fBBUGS\fP, below).
.PP
However, this option has one significant drawback: changes to the source files
can cause the line numbers recorded in the tag file to no longer correspond
to the lines in the source file, causing jumps to some tags to miss the target
definition by one or more lines. Basically, this option is best used when the
source code to which it is applied is not subject to change. Selecting this
option type causes the following options to be ignored: \fB-BF\fP.
.RE
.PD 1
.TP 9
.I pattern
Uses EX search patterns for all tags, rather than the line numbers usually
used for macro definitions. This has the advantage of not referencing obsolete
line numbers when lines have been added or removed since the tag file was
generated.
.TP 9
.I mixed
Uses line numbers for macro definition tags and EX patterns for everything
else. This is the default format generated by the original \fBctags\fP and is,
therefore, retained as the default for this option.
.RE

.TP 5
.BI \--format= level
Change the format of the output tag file. Currently the only valid values for
\fIlevel\fP are \fI1\fP or \fI2\fP. Level 1 specifies the original tag file
format and level 2 specifies a new extended format containing extension flags
(but in a manner which retains backward compatibility with original
\fBvi\fP(1) implementations). The default level is 2.

.TP 5
.B \--help
Prints to standard output a detailed usage description.

.TP 5
\fB--if0=\fIyes\fR|\fIno\fR
Indicates a preference as to whether code within an "#if 0" branch of a
preprocessor conditional should be examined for non-macro tags (macro tags are
always included). Because the intent of this construct is to disable code, the
default value of this options is \fIno\fP. Note that this indicates a
preference only and does not guarantee skipping code within an "#if 0" branch,
since the fall-back algorithm used to generate tags when preprocessor
conditionals are too complex follows all branches of a conditional. If the
parameter is omitted, \fB=\fIyes\fR is implied. This option is off by default.

.TP 5
\fB--lang=\fIc\fR|\fIc++\fR|\fIjava\fR
By default, \fBctags\fP automatically selects the language of a source file
according to its file name extension, ignoring those files whose extensions
are unknown to \fBctags\fP. This option forces the specified language to be
used for every supplied file, instead of keying off of their extensions.

.TP 5
\fB--langmap=\fImap(s)\fR
Overrides the default mapping between source language and file extension. Each
comma-separated \fImap\fP consists of the source language name, a colon, and a
list of extensions separated by periods. For example, to specify that files
with extensions of .c, .ec, and .xs are to be treated as C language files, use
"\fB--langmap=\fP\fIc:.c.ec.xs\fP". To also specify that files with extensions
of .j are to be treated as Java language files, use
"\fB--langmap=\fP\fIc:.c.ec.xs,java:.java.j\fP".

.TP 5
\fB--recurse=\fIyes\fR|\fIno\fR
Recurse into directories encountered in the list of supplied files. If the
list of supplied files is empty and no file list is specified with the
\fB-L\fP option, then the current directory (i.e. ".") is assumed. On Unix,
directories named "SCCS" are skipped, because files in these directories are
not source code, even though they have the same names as the source code they
relate to. Also on Unix, symbolic links are followed; if you don't like this,
pipe the output of \fBfind\fP(1) into \fBctags -L-\fP instead. \fBNote:\fP
This option is not supported on all platforms at present.

.TP 5
\fB--sort=\fIyes\fR|\fIno\fR
Indicates whether the tag file should be sorted on the tag name (default is
\fIyes\fP). Note that the original \fBvi\fP(1) requires sorted tags. If the
parameter is omitted, \fB=\fIyes\fR is implied. This option is on by default
for \fBctags\fR, and ignored for \fBetags\fR.

.TP 5
\fB--totals=\fIyes\fR|\fIno\fR
Prints statistics about the source files read and the tag file written during
the current invocation of \fBctags\fP. If the parameter is omitted,
\fB=\fIyes\fR is implied. This option is off by default.

.TP 5
.B \--version
Prints a version identifier for \fBctags\fP to standard output. This is
guaranteed to always contain the string "Exuberant Ctags".


.SH "OPERATIONAL DETAILS"
For every one of the qualified objects which are discovered in the source
files supplied to \fBctags\fP, a separate line is added to the tag file, each
looking like this in the most general case:
.PP
.RS 4
tag_name    file_name    ex_cmd;"    xflags
.RE
.PP
The fields and separators of these lines are specified as follows:
.PP
.PD 0
.RS 4
.TP 4
1.
tag name (a C language identifier)
.TP 4
2.
a single tab character
.TP 4
3.
the name of the file in which the object associated with the tag is located
.TP 4
4.
a single tab character
.TP 4
5.
an EX command to locate the tag within the file; generally a search pattern
(either /pattern/ or ?pattern?) or line number (see \fB--excmd\fP). Tag file
format 2 (see \fB--format\fP) extends this EX command under certain
circumstances to include a set of extension flags (see \fBEXTENSION FLAGS\fP,
below) embedded in an EX comment immediately appended to the EX command, which
leaves it backwards compatible with original \fBvi\fP(1) implemenations.
.RE
.PD 1
.PP
A few special tags are written into the tag file for internal purposes. These
tags are composed in such a way that they always sort to the top of the file.
Therefore, the first two characters of these tags are used a magic number to
detect a tag file for purposes of determining whether a valid tag file is
being overwritten rather than a source file.
.PP
When this program is invoked by the name \fBetags\fP, or with the \fB-e\fP
option, the output file is in a different format that is used by \fBemacs\fP(1).
.PP
Note that the name of each source file will be recorded in the tag file
exactly as it appears on the command line. Therefore, if the path you
specified on the command line was relative to some directory, then it will
be recorded in that same manner in the tag file.
.PP
This version of \fBctags\fP imposes no formatting requirements. Other versions
of ctags tended to rely upon certain formatting assumptions in order to help
it resolve coding dilemmas caused by preprocessor conditionals.
.PP
In general, \fBctags\fP tries to be smart about conditional preprocessor
directives. If a preprocessor conditional is encountered within a statement
which defines a tag, \fBctags\fP follows only the first branch of that
conditional (except in the special case of "#if 0", in which case it follows
only the last branch). The reason for this is that failing to pursue only one
branch can result in ambiguous syntax, as in the following example:
.PP
.RS
#ifdef TWO_ALTERNATIVES
.br
struct {
.br
#else
.br
union {
.br
#endif
.RS 4
short a;
.br
long b;
.RE
}
.RE
.PP
Both branches cannot be followed, or braces become unbalanced and \fBctags\fP
would be unable to make sense of the syntax.
.PP
If the application of this heuristic fails to properly parse a file,
generally due to complicated and inconsistent pairing within the conditionals,
\fBctags\fP will retry the file using a different heuristic which does not
selectively follow conditional preprocessor branches, but instead falls back
to relying upon a closing brace ("}") in column 1 as indicating the end of a
block once any brace imbalance results from following a #if conditional branch.
.PP
\fBCtags\fP will also try to specially handle arguments lists enclosed in
double sets of parentheses in order to accept the following conditional
construct:
.PP
.RS
extern void foo __ARGS((int one, char two));
.RE
.PP
Any name immediately preceding the "((" will be automatically ignored and
the previous name will be used.
.PP
C++ operator definitions are specially handled. In the case of operators
beginning with a non-identifer character (e.g. "operator ="), the name of the
tag added to the tag file will be the operator prefixed by "operator", with no
intervening white space (e.g. "operator="). In the case of operators beginning
with identifer characters (e.g. "operator new"), the name of the tag added to
the tag file will be simply the name of the operator, without a prefix (e.g.
"new").
.PP
After creating or appending to the tag file, it is sorted by the tag name,
removing identical tag lines.
.PP
Note that the path recorded for filenames in the tag file and utilized by
the editor to search for tags are identical to the paths specified for
\fIfile(s)\fP on the command line. This means the if you want the paths for
files to be relative to some directory, you must invoke \fBctags\fP with the
same pathnames for \fIfile(s)\fP (this can be overridden with \fB-p\fP).

.SH "EXTENSION FLAGS"
Extension flags are tab-separated key-value pairs appended to the end of the
EX command as a comment, as described above in \fBOPERATIONAL DETAILS\fP.
These key value pairs appear in the general form "\fIkey\fP:\fIvalue\fP".
The possible keys and the meaning of their values are as follows:
.PP

.TP 12
.I class
Indicates that this tag is a member of the class whose name is given by
\fIvalue\fP.
.PP

.TP 12
.I enum
Indicates that this tag is a member of the enumeration whose name is given by
\fIvalue\fP.
.PP

.TP 12
.I file
Indicates that the tag has a file-limited scope (i.e. is static to the file).
This key has no corresponding value.
.PP

.TP 12
.I kind
Indicates the type of the tag. Its value is one of the corresponding
one-letter flags described under the \fB-i\fP option, above. Alternatively,
this key may be omitted, with only the value present (i.e. a field without a
':' defaults to the \fBkind\fP key).
.PP

.TP 12
.I interface
Indicates that this tag is a member of the interface whose name is given by
\fIvalue\fP.
.PP

.TP 12
.I namespace
Indicates that this tag is a member of the namespace whose name is given by
\fIvalue\fP.
.PP

.TP 12
.I private
Indicates the visibility of this class member is private.
This key has no corresponding value.

.TP 12
.I protected
Indicates the visibility of this class member is protected.
This key has no corresponding value.

.TP 12
.I public
Indicates the visibility of this class member is public.
This key has no corresponding value.

.TP 12
.I struct
Indicates that this tag is a member of the structure whose name is given by
\fIvalue\fP.
.PP

.TP 12
.I union
Indicates that this tag is a member of the union whose name is given by
\fIvalue\fP.
.PP


.SH "ENVIRONMENT VARIABLES"
.TP 8
.B CTAGS
This variable, if found, will be assumed to contain a set of custom default
options which are read when \fBctags\fP starts, but before any command line
options are read. Options in this variable should be in the same form as those
on the command line. Command line options will override options specified in
this variable. Only options may be specified with this variable; no source
file names are read from its value.
.PP
.TP 8
.B ETAGS
Similar to the \fBCTAGS\fP variable above, this variable, if found, will be
read when \fBetags\fP starts. If this variable is not found, \fBetags\fP will
try to use \fBCTAGS\fP instead.

.SH "HOW TO USE WITH VI"
Vi will, by default, expect a tag file by the name "tags" in the current
directory. Once the tag file is built, the following commands exercise the tag
indexing feature:
.TP 12
.B vi -t tag
Start vi and position the cursor at the file and line where "tag" is defined.
.TP 12
.B Control-]
Find the tag under the cursor.
.TP 12
.B :ta tag
Find a tag.
.TP 12
.B Control-T
Return to previous location before jump to tag (not widely implemented).


.SH "HOW TO USE WITH GNU EMACS"
Emacs will, by default, expect a tag file by the name "TAGS" in the current
directory. Once the tag file is built, the following commands exercise the
tag indexing feature:
.TP 12
.B "Meta-x visit-tags-table"
Visit a TAGS file.
.TP 12
.B "Meta-."
Find a definition for a tag.  The default tag is the identifier under the
cursor.  There is name completion in the minibuffer; typing \fB"foo TAB"\fP
completes the identifier starting with `foo' (`foobar', for example) or lists
the alternatives.
.TP 12
.B "Meta-,"
Find the next definition for the tag.  Exact matches are found first, followed
by fuzzier matches.
.PP
For more commands, see the
.I Tags
topic in the Emacs info tree.


.SH BUGS
Because \fBctags\fP does not look inside brace-enclosed function blocks, local
definitions of objects within a function will not have tags generated for them.
.PP
Legacy C source code which uses C++ reserved keywords as variable or parameter
names (e.g. "class", etc.) in a header file may fail to have correct tags
generated for the objects using them. In order to properly handle such code,
use the \fB--lang\fP option.
.PP
Note that when \fBctags\fP generates uses patterns for locating tags (see
the \fB--excmd\fP option), it is entirely possible that the wrong line may be
found by your editor if there exists another source line which is identical to
the line containing the tag. The following example demonstrates this condition:
.PP
.RS
int variable;

/* ... */
.br
void foo(variable)
.br
int variable;
.br
{
.RS 4
/* ... */
.RE
}
.RE
.PP
Depending upon which editor you use and where in the code you happen to be, it
is possible that the search pattern may locate the local parameter declaration
in foo() before it finds the actual global variable definition, since the
lines (and therefore their search patterns are identical). This can be avoided
by use of the \fB--excmd=n\fP option.
.PP
Because \fBctags\fP is neither a preprocessor nor a compiler, some complex
or obscure constructs can fool \fBctags\fP into either missing a tag or
improperly generating an inappropriate tag. In particular, the use of
preprocessor constructs which alter the textual syntax of C can fool
\fBctags\fP, as demonstrated by the following example:
.PP
.RS
#ifdef GLOBAL
.br
#define EXTERN
.br
#define INIT(assign)  assign
.br
#else
.br
#define EXTERN extern
.br
#define INIT(assign)
.br
#endif
.br

EXTERN BUF *firstbuf INIT(= NULL);
.RE
.PP
This looks too much like a declaration for a function called "INIT", which
returns a pointer to a typedef "firstbuf", rather than the actual variable
definition that it is, since this distinction can only be resolved by the
preprocessor. The moral of the story: don't do this if you want a tag
generated for it, or use the \fB-I\fP option to specify "INIT" as a keyword
to be ignored.


.SH FILES
.TP 10
.I tags
The default tag file created by \fBctags\fP.
.TP 10
.I TAGS
The default tag file created by \fBetags\fP.

.SH "SEE ALSO"
The official Exuberant Ctags web site at:
.PP
.RS 4
http://darren.hiebert.com/ctags/index.html
.RE
.PP
Also \fBex\fP(1), \fBvi\fP(1), \fBelvis\fP, or, better yet, \fBvim\fP, the
official editor of \fBctags\fP. For more information on \fBvim\fP, see the VIM
Pages web site at:
.PP
.RS 4
http://www.vim.org/
.RE


.SH AUTHOR
Darren Hiebert <darren@hiebert.com>, <darren@hiwaay.net>
.br
http://darren.hiebert.com/


.SH MOTIVATION
"Think ye at all times of rendering some service to every member of the human
race."
.PP
"All effort and exertion put forth by man from the fullness of his heart is
worship, if it is prompted by the highest motives and the will to do service
to humanity."
.PP
.RS 10
\-- From the Baha'i Writings


.SH CREDITS
This version of \fBctags\fP was originally derived from and inspired by the
ctags program by Steve Kirkendall <kirkenda@cs.pdx.edu> that comes with the
Elvis vi clone (though virtually none of the original code remains).
.PP
Credit is also due Bram Moolenaar <mool@oce.nl>, the author of \fBvim\fP, who
has devoted so much of his time and energy both to developing the editor as a
service to others, and to helping the orphans of Uganda. 
.PP
The section entitled "HOW TO USE WITH GNU EMACS" was shamelessly stolen from
the man page for GNU \fBetags\fP.
