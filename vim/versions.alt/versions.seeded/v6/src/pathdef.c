/* pathdef.c */
/* This file is automatically created by Makefile
 * DO NOT EDIT!  Change Makefile only. */
#include "vim.h"
char_u *default_vim_dir = (char_u *)"/home/repository/vim/vim-5.7/src/share/vim";
char_u *default_vimruntime_dir = (char_u *)"";
char_u *all_cflags = (char_u *)"gcc -c -I. -Iproto -DHAVE_CONFIG_H     -g -O2 -Wall     ";
char_u *all_lflags = (char_u *)"gcc  -o vim         -lncurses  -lgpm    ";
char_u *compiled_user = (char_u *)"repository";
char_u *compiled_sys = (char_u *)"experiment";
