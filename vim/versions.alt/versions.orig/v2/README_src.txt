README_src.txt for version 5.3 of Vim: Vi IMproved.

This is the source archive for Vim.  It is packed for Unix systems.  It is
also used for other systems in combination with the extra archive
(vim-5.2-extra.tar.gz).

For more information, see the README.txt file that comes with the runtime
archive (vim-5.2-rt.tar.gz).  To be able to run Vim you MUST get the runtime
archive too!
